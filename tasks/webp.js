import gulp from 'gulp';
import cwebp from 'gulp-cwebp';
import changed from 'gulp-changed';
import { distDir } from '../gulpfile.babel';

const path = require('path');

gulp.task('webp', (done) => {
  gulp.src('src/resources/images/**/*.{jpg,jpeg,png}')
    .pipe(cwebp())
    .pipe(gulp.dest(`${distDir}/images/`));
  done();
});
