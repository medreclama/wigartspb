const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';

module.exports = {
  mode,
  entry: {
    script: './src/pages/index/script.js',
    'script-home': './src/pages/home/script-home.js',
    'script-ui-kit': './src/pages/ui-kit/script-ui-kit.js',
    'script-catalog': './src/pages/catalog/script-catalog.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].js',
  },
  devtool: isProduction ? undefined : 'source-map',
  plugins: [new ESLintPlugin()],
  optimization: {
    minimize: isProduction,
  },
};
