import { CatalogFilter } from '../../blocks/catalog-filter/catalog-filter';
import lb from '../../common/js/modules/lightbox';
import { tabs } from '../../blocks/tabs/tabs';
import mrcSH from '../../blocks/show-hide/show-hide';
import submitForm from '../../blocks/form/form';
import Slider from '../../blocks/slider/slider';

window.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new CatalogFilter();
  lb();
  tabs();
  mrcSH();
  submitForm('/callback.php');
  const productPhotosList = document.querySelector('.product-photos__list .slider__slides');
  if (productPhotosList) {
    const productPhotosListInstance = new Slider(productPhotosList, {
      spaceBetween: 20,
      breakpoints: {
        300: {
          slidesPerView: 3,
        },
        480: {
          slidesPerView: 2,
        },
        1000: {
          slidesPerView: 4,
        },
      },
    });
    productPhotosListInstance.init();
  }
});
