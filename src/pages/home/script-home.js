import { homeContacts } from '../../blocks/home-contacts/home-contacts';
import parallax from '../../common/js/modules/parallax';
import appear from '../../blocks/appear/appear';
import submitForm from '../../blocks/form/form';
import iframeLazyInit from '../../blocks/iframeLazy/iframeLazy';
import { homeProductInit } from '../../blocks/home-product/home-product';
import lb from '../../common/js/modules/lightbox';
import { homePurchase } from '../../blocks/home-purchase/home-purchase';
import Slider from '../../blocks/slider/slider';
import ratingInit from '../../blocks/rating/rating';
import { homeFeedback } from '../../blocks/home-feedback/home-feedback';
import { homePlatforms } from '../../blocks/home-platforms/home-platforms';

window.Slider = Slider;

window.addEventListener('DOMContentLoaded', () => {
  iframeLazyInit('home-iframe');
  homeContacts.init();
  appear();
  submitForm();
  lb();
  homePurchase();
  const homeHeader = document.querySelector('.home-header');
  const homeDescriptionImage = document.querySelector('.home-description-image');
  const homeVideo = document.querySelector('.home-video');
  const homeStage = document.querySelector('.home-stages');
  const homeStageItems = document.querySelectorAll('.home-stages__item');
  const homeProductsList = document.querySelector('.home-products-list');
  homeProductInit();
  homePlatforms();
  homeFeedback();
  ratingInit();
  parallax(homeHeader, '--before-translate-y', 200, -200);
  parallax(homeHeader, '--after-translate-y', 400, -200);
  parallax(homeDescriptionImage, '--image-1-translate-y', -50, 100);
  parallax(homeDescriptionImage, '--image-2-translate-y', 25, -50);
  parallax(homeVideo, '--iframe-translate-y', -25, 50);
  parallax(homeVideo, '--description-translate-y', 25, -50);
  parallax(homeStage, '--before-translate-y', 100, -200);
  parallax(homeStage, '--after-translate-y', 400, -200);
  homeStageItems.forEach((item) => {
    parallax(item, '--item-before-translate-y', 20, -40);
    parallax(item, '--item-after-translate-y', 20, -40);
  });
  parallax(homeProductsList, '--after-translate-y', 400, -200);
});
