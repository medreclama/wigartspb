export const homePurchase = () => {
  const items = document.querySelectorAll('.home-purchase__item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      if (event.target.closest('.home-purchase__title--is-openable')) item.classList.toggle('home-purchase__item--active');
    });
  });
};
