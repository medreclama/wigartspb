const initMap = () => {
  const iconSize = [41, 75];
  const iconOffset = [-20, -65];

  // eslint-disable-next-line no-undef
  const myMap = new ymaps.Map('home-contacts-map', {
    center: [59.908085, 30.312778],
    controls: ['zoomControl'],
    zoom: 16,
  }, {
    searchControlProvider: 'yandex#search',
  });

  myMap.behaviors.disable('scrollZoom');

  const balloon = 'наб. Обводного канала, 108';

  // Добавление меток на карту
  myMap.geoObjects.add(
    // eslint-disable-next-line no-undef
    new ymaps.Placemark(
      [59.908085, 30.312778],
      {
        balloonContent: balloon,
        iconCaption: balloon,
      },
      {
        iconLayout: 'default#image',
        iconImageHref: '/local/templates/wigart/images/pin.svg',
        // iconImageHref: '/bitrix/templates/tpl_medall/images/pin.svg', // Своё изображение иконки метки
        iconImageSize: iconSize, // Размеры метки
        iconImageOffset: iconOffset, // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
      },
    ),
  );
};

class HomeContacts {
  #element;
  #isYandexMapShowed;

  constructor(element) {
    this.#element = element;
  }

  #scrollHandler = () => {
    if (!this.#isYandexMapShowed) {
      // eslint-disable-next-line no-undef
      ymaps.ready(initMap);
      this.#isYandexMapShowed = true;
      window.removeEventListener('scroll', this.#scrollHandler);
    }
  };

  init = () => window.addEventListener('scroll', this.#scrollHandler);
}

export const homeContacts = new HomeContacts(document.querySelector('.home-contacts'));
