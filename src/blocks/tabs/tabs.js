const getHash = () => window.location.hash.substring(1);

export class Tabs {
  #element;
  #labels;
  #labelsArray;
  #content;
  #hashes;

  constructor(element) {
    this.#element = element;
    this.#labels = this.#element.querySelectorAll('.tabs__label');
    this.#labelsArray = Array.from(this.#labels);
    this.#hashes = this.#labelsArray.map((label) => ({
      id: label.id,
      index: this.#labelsArray.indexOf(label),
    })).filter((label) => label.id !== '');
    this.#content = this.#element.querySelectorAll('.tabs__item');
  }

  #setActiveTab = (index) => {
    if (this.#labels[index].classList.contains('tabs__label--active')) return;
    this.#labels.forEach((label, i) => {
      const tab = this.#content[i];
      label.classList.remove('tabs__label--active');
      tab.classList.remove('tabs__item--active');
      if (+i === index) {
        label.classList.add('tabs__label--active');
        tab.classList.add('tabs__item--active');
      }
    });
  };

  #setLinksForHash = () => {
    this.#hashes.forEach((hash) => {
      const links = document.querySelectorAll(`[href="#${hash.id}"]`);
      links.forEach((link) => {
        // console.log(link);
        link.addEventListener('click', () => this.#setActiveTab(hash.index));
      });
    });
  };

  #labelClickHandler = (event) => {
    // event.preventDefault();
    const { target } = event;
    const index = this.#labelsArray.indexOf(target);
    if (index >= 0) this.#setActiveTab(index);
  };

  #changeTabOnHashChange = () => {
    const label = this.#labelsArray.find((l) => l.id === getHash());
    const index = this.#labelsArray.indexOf(label);
    if (index >= 0) this.#setActiveTab(index);
  };

  #changeTabOnLoadByHash = () => {
    const index = this.#hashes.find((hash) => hash.id === getHash())?.index;
    if (index) this.#setActiveTab(index);
  };

  init = () => {
    this.#setActiveTab(0);
    this.#element.addEventListener('click', this.#labelClickHandler);
    this.#setLinksForHash();
    window.addEventListener('hashchange', this.#changeTabOnHashChange);
    window.addEventListener('DOMContentLoaded', this.#changeTabOnLoadByHash);
  };
}

export const tabs = () => {
  const tabsList = document.querySelectorAll('.tabs');
  tabsList.forEach((item) => {
    const tabsInstance = new Tabs(item);
    tabsInstance.init();
  });
};
