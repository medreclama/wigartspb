import noUiSlider from 'nouislider';

export class CatalogFilter {
  static MAX_PRICE;
  #element = document.querySelector('.catalog-filter');
  #items = this.#element?.querySelectorAll('.catalog-filter__item--is-openable');
  #elementActiveClass = 'catalog-filter--active';
  #titleActiveClass = 'catalog-filter__item--active';
  #mobileButton = this.#element?.querySelector('.catalog-filter__mobile');
  #priceRangeElement = this.#element?.querySelector('.catalog-filter__price-range');
  #priceFrom = document.getElementById('price-from');
  #priceTo = document.getElementById('price-to');
  #pricesInputs = [this.#priceFrom, this.#priceTo];

  constructor() {
    if (this.#priceRangeElement) CatalogFilter.MAX_PRICE = Number(this.#priceRangeElement.dataset.maxPrice);
    this.init();
  }

  #showHide() {
    this.#element.addEventListener('click', (event) => {
      if (event.target === this.#mobileButton) this.#element.classList.toggle(this.#elementActiveClass);
      this.#mobileButton.innerHTML = this.#element.classList.contains(this.#elementActiveClass) ? 'Скрыть фильтр' : 'Показать фильтр';
    });
    this.#items.forEach((item) => {
      item.addEventListener('click', (event) => {
        if (event.target.closest('.catalog-filter__title')) item.classList.toggle(this.#titleActiveClass);
      });
    });
  }

  #initNoUiSlider = () => {
    noUiSlider.create(this.#priceRangeElement, {
      range: {
        min: 0,
        max: CatalogFilter.MAX_PRICE,
      },
      connect: true,
      start: [
        this.#priceFrom.value ? parseFloat(this.#priceFrom.value) : 0,
        this.#priceTo.value ? parseFloat(this.#priceTo.value) : CatalogFilter.MAX_PRICE,
      ],
      step: 1,
      format: {
        to: (value) => value.toFixed(0),
        from: (value) => parseFloat(value),
      },
    });
  };

  #updateInputValues() {
    this.#priceRangeElement.noUiSlider.on('update', (values, handle) => {
      this.#pricesInputs[handle].value = values[handle];
      if (values[handle] === '0') this.#pricesInputs[handle].value = 0;
      else if (values[handle] === `${CatalogFilter.MAX_PRICE}`) this.#pricesInputs[handle].value = `${CatalogFilter.MAX_PRICE}`;
    });
  }

  #updateRangeValues() {
    this.#pricesInputs.forEach((input, handle) => {
      input.addEventListener('change', () => {
        if (input.value === '' && handle === 0) this.#priceRangeElement.noUiSlider.set([0, true]);
        else if (input.value === '' && handle === 1) this.#priceRangeElement.noUiSlider.set([true, CatalogFilter.MAX_PRICE]);
        this.#priceRangeElement.noUiSlider.setHandle(handle, input.value);
      });
    });
  }

  init() {
    if (!this.#element) return;
    this.#showHide();
    this.#initNoUiSlider();
    this.#updateInputValues();
    this.#updateRangeValues();
  }
}
