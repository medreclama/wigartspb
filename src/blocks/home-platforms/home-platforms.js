const homePlatformsSlider = document.querySelector('.home-platforms .slider__slides');

export const homePlatforms = () => {
  if (homePlatformsSlider) {
    const homePlatformsInstance = new window.Slider(homePlatformsSlider, {
      spaceBetween: 40,
      slidesPerView: 1.3,
      breakpoints: {
        480: {
          slidesPerView: 2,
        },
        1000: {
          slidesPerView: 4,
          spaceBetween: 0,
        },
      },
    });
    homePlatformsInstance.init();
  }
};
