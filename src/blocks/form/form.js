// eslint-disable-next-line import/no-extraneous-dependencies
import IMask from 'imask';
import { showModal } from '../modal/modal';

const forms = document.querySelectorAll('.form');
const tel = document.querySelectorAll('.form-input__field[type="tel"]');

const maskOptions = {
  mask: '+{7} (900) 000-00-00',
};

tel.forEach((field) => new IMask(field, maskOptions));

const showMessage = (text) => {
  const message = document.createElement('div');
  message.className = 'modal';
  message.innerHTML = `
    <div class="modal__inner">
      <p>${text}</p>
      <button type="button" class="modal__close"></button>
    </div>
    `;
  document.body.append(message);
  showModal(message);
  message.addEventListener('modalClose', () => message.remove());
};

const sendData = (url, onSuccess, onFail, body) => {
  fetch(url, {
    method: 'POST',
    body,
  })
    .then((response) => {
      if (response.ok) {
        response.json();
        onSuccess();
      } else onFail();
    })
    .catch(() => onFail());
};

const submitForm = (url = '/ajax.php') => { // для главной формы
  forms.forEach((form) => {
    form.addEventListener('submit', (evt) => {
      const onSuccess = form.dataset.answerSuccess ? () => showMessage(form.dataset.answerSuccess) : () => showMessage('Благодарим вас за заявку! В ближайшее время мы свяжемся с вами, чтобы ответить на ваши вопросы и подобрать время консультации.');

      const onFail = () => showMessage('Произошла ошибка, перезагрузите страницу');
      evt.preventDefault();
      sendData(
        url,
        () => {
          onSuccess();
          form.reset();
        },
        () => {
          onFail();
        },
        new FormData(evt.target),
      );
    });
  });
};

export default submitForm;
