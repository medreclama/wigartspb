class HomeProduct {
  static list = [];

  #element;
  #video;
  #playingClass = 'home-product--playing';

  constructor(element) {
    if (!element) return;
    this.#element = element;
    this.#video = this.#element?.querySelector('.home-product__video');
  }

  pause = () => {
    this.#element.classList.remove(this.#playingClass);
    this.#video?.pause();
  };

  #clickHandler = () => {
    const hasClass = this.#element.classList.contains(this.#playingClass);
    HomeProduct.list.forEach((homeProduct) => homeProduct.pause());

    if (!hasClass) {
      this.#element.classList.add(this.#playingClass);
      this.#video.play();
    }
  };

  init = () => {
    if (this.#video) {
      this.#element.addEventListener('click', this.#clickHandler);
    }
    HomeProduct.list.push(this);
  };
}

export const homeProductInit = () => {
  const elements = document.querySelectorAll('.home-product');
  elements.forEach((element) => {
    const homeProduct = new HomeProduct(element);
    homeProduct.init();
  });
};
