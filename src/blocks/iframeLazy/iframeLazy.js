const iframeLazyInit = (wrapper) => {
  const createYoutubeIframe = (wrapperId) => {
    const iframeWrapper = document.getElementById(wrapperId);
    if (iframeWrapper) {
      const iframe = document.createElement('iframe');
      iframe.src = `${iframeWrapper.dataset.src}`;
      iframe.setAttribute('allowfullscreen', 'allowfullscreen');
      iframeWrapper.appendChild(iframe);
    }
  };

  const userEventsInit = () => {
    window.removeEventListener('scroll', userEventsInit);
    createYoutubeIframe(wrapper);
  };

  window.addEventListener('scroll', userEventsInit);
};

export default iframeLazyInit;
