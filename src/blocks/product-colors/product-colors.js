const colorsList = document.querySelector('.product-colors__list');
const colors = document.querySelectorAll('.product-colors__image');
const color = document.querySelector('.product-colors__current span');
const availability = document.querySelector('.product-price .block-marked p');

export const productColorsInit = () => {
  if (!colorsList) return;
  if (colors[0].dataset.availability === 'available') availability.innerHTML = 'В наличии';
  else if (colors[0].dataset.availability === 'not-available') availability.innerHTML = 'Нет в наличии, возможен повтор';
  color.innerHTML = colors[0].dataset.color;
  colors[0].classList.add('product-colors__image--active');
  colorsList.addEventListener('click', (evt) => {
    if (evt.target.classList.contains('product-colors__image')) {
      colors.forEach((item) => item.classList.remove('product-colors__image--active'));
      evt.target.classList.add('product-colors__image--active');
      color.innerHTML = evt.target.dataset.color;
      if (evt.target.dataset.availability === 'available') availability.innerHTML = 'В наличии';
      else if (evt.target.dataset.availability === 'not-available') availability.innerHTML = 'Нет в наличии, возможен повтор';
    }
  });
};
