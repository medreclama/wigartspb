const homeFeedbackElement = document.querySelector('.home-feedback');
const homeFeedbackSlider = homeFeedbackElement.querySelector('.slider__slides');
const homeFeedbackItems = homeFeedbackElement.querySelectorAll('.home-feedback__item');

export const homeFeedback = () => {
  if (homeFeedbackSlider) {
    const homeFeedbackInstance = new window.Slider(homeFeedbackSlider, {
      spaceBetween: 40,
      slidesPerView: 1,
      pagination: {
        el: '.slider__pagination',
        dynamicBullets: true,
      },
      breakpoints: {
        800: {
          slidesPerView: 2,
        },
        1000: {
          slidesPerView: 2,
        },
      },
    });
    homeFeedbackInstance.init();
    homeFeedbackItems.forEach((item) => {
      const homeFeedbackText = item.querySelector('.home-feedback__text');
      const homeFeedbackLink = item.querySelector('.home-feedback__link');
      if (homeFeedbackText.scrollHeight > homeFeedbackText.offsetHeight) {
        homeFeedbackLink.classList.add('home-feedback__link--active');
      }
    });
    homeFeedbackElement.addEventListener('click', (evt) => {
      evt.preventDefault();
      if (evt.target.classList.contains('home-feedback__link')) {
        const feedbackItem = evt.target.closest('.home-feedback__item');
        feedbackItem.classList.toggle('home-feedback__item--active');
        // eslint-disable-next-line no-param-reassign
        evt.target.textContent = feedbackItem.classList.contains('home-feedback__item--active') ? 'Скрыть' : 'Показать еще';
      }
    });
  }
};
